package com.timofeev.githubapp.data.storage;

import com.timofeev.githubapp.data.network.res.PeopleRes;

public class PeopleDto {

    private String mUrlAvatar;
    private String mLogin;

    public PeopleDto(String urlAvatar, String login) {
        mUrlAvatar = urlAvatar;
        mLogin = login;
    }

    public PeopleDto(PeopleRes peopleRes) {
        mUrlAvatar = peopleRes.getAvatarUrl();
        mLogin = peopleRes.getLogin();
    }

    public String getUrlAvatar() {
        return mUrlAvatar;
    }

    public String getLogin() {
        return mLogin;
    }
}
