package com.timofeev.githubapp.data.network.errors;

public class MuteServer extends Throwable {
    public MuteServer() {
        super("Ошибка 500: сервер не отвечает");
    }
}
