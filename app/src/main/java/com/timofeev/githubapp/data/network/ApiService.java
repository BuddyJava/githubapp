package com.timofeev.githubapp.data.network;

import com.timofeev.githubapp.data.network.res.PeopleRes;
import com.timofeev.githubapp.data.network.res.PersonRes;
import com.timofeev.githubapp.data.storage.PeopleDto;

import java.util.List;

import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface ApiService {

    @GET("users/whatever/followers")
    Observable<Response<List<PeopleRes>>> getPeopleObs(@Query("page") int page,
                                                       @Query("per_page") int count);

    @GET("users/{login}")
    Observable<Response<PersonRes>> getPersonObs(@Path("login") String login);

}
