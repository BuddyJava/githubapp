package com.timofeev.githubapp.data.network;


import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.timofeev.githubapp.data.network.errors.EmptyPage;
import com.timofeev.githubapp.data.network.errors.ErrorUtils;
import com.timofeev.githubapp.data.network.errors.MuteServer;
import com.timofeev.githubapp.data.network.errors.NetworkAvailableError;
import com.timofeev.githubapp.utills.L;
import com.timofeev.githubapp.utills.NetworkStatusChecker;

import retrofit2.Response;
import rx.Observable;

public class ResponseCallTransformer<R> implements Observable.Transformer<Response<R>, R> {

    @Override
    @RxLogObservable
    public Observable<R> call(Observable<Response<R>> responseObservable) {
        return NetworkStatusChecker.isInternetAvailable()
                .flatMap(aBoolean -> aBoolean ? responseObservable : Observable.error(new NetworkAvailableError()))
                .flatMap(rResponse -> {
                    switch (rResponse.code()) {
                        case 200:

                            return Observable.just(rResponse.body());

                        case 404:
                            return Observable.error(new EmptyPage());
                        case 500:
                            return Observable.error(new MuteServer());
                        default:
                            return Observable.error(ErrorUtils.parseError(rResponse));
                    }
                });
    }

}

