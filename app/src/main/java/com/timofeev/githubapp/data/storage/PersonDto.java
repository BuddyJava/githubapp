package com.timofeev.githubapp.data.storage;

import com.timofeev.githubapp.data.network.res.PersonRes;

public class PersonDto {

    private String mAvatar;
    private String mCompany;
    private String mLogin;
    private String mName;
    private String mEmail;
    private String mBio;
    private String mLocation;

    public PersonDto(PersonRes res) {
        mLogin = res.getLogin();
        mName = res.getName();
        if(res.getCompany() != null)mEmail = ((String) res.getEmail());
        if(res.getCompany() != null)mBio = ((String ) res.getBio());
        if(res.getCompany() != null)mLocation = ((String ) res.getLocation());
        mAvatar = res.getAvatarUrl();
        if(res.getCompany() != null)mCompany = ((String) res.getCompany());
    }

    public String getAvatar() {
        return mAvatar;
    }

    public String getCompany() {
        return mCompany;
    }

    public String getLogin() {
        return mLogin;
    }

    public String getName() {
        return mName;
    }

    public String getEmail() {
        return mEmail;
    }

    public String getBio() {
        return mBio;
    }

    public String getLocation() {
        return mLocation;
    }
}
