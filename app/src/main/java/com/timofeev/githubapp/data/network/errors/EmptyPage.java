package com.timofeev.githubapp.data.network.errors;

/**
 * Created by Anastasiya on 06.02.2017.
 */

public class EmptyPage extends Throwable {
    public EmptyPage() {
        super("Ошибка 404: страница не найдена");
    }
}
