package com.timofeev.githubapp.data.network.errors;

/**
 * Created by Anastasiya on 05.02.2017.
 */

public class NetworkAvailableError extends Throwable{

    public NetworkAvailableError() {
        super("Соедидение отсутсвует, попробуйте позже");
    }
}
