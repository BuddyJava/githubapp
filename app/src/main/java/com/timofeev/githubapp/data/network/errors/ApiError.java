package com.timofeev.githubapp.data.network.errors;

class ApiError extends Throwable {
    private int statusCode;
    private String message;

    @Override
    public String getMessage() {
        return message;
    }
}
