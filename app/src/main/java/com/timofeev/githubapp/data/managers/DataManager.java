package com.timofeev.githubapp.data.managers;

import com.timofeev.githubapp.data.network.ApiService;
import com.timofeev.githubapp.data.network.ResponseCallTransformer;
import com.timofeev.githubapp.data.storage.PeopleDto;
import com.timofeev.githubapp.data.storage.PersonDto;
import com.timofeev.githubapp.di.Injector;
import com.timofeev.githubapp.di.components.DaggerDataManagerComponent;
import com.timofeev.githubapp.di.modules.LocalModule;
import com.timofeev.githubapp.di.modules.NetworkModule;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class DataManager {


    @Inject
    ApiService mService;

    @Inject
    Retrofit mRetrofit;


    private static DataManager INSTANCE = null;

    //region ============= CONSTRUCTOR ===============
    private DataManager() {
        DaggerDataManagerComponent.builder()
                .appComponent(Injector.getAppComponent())
                .localModule(new LocalModule())
                .networkModule(new NetworkModule())
                .build()
                .inject(this);
    }

    public static DataManager getInstance() {

        if (INSTANCE == null) {
            INSTANCE = new DataManager();
        }
        return INSTANCE;
    }
    //endregion

    //region ============= GETTERS ===============
    public Retrofit getRetrofit(){
        return mRetrofit;
    }

    public Observable<PeopleDto> getPeopleObs(int page) {


//        return Observable.from(mMockList);
        return mService.getPeopleObs(page, 20)
                .compose(new ResponseCallTransformer<>())
                .flatMap(Observable::from)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(PeopleDto::new)
                .toList()
                .flatMap(Observable::from);
    }

    public Observable<PersonDto> getPersonObs(String login){
        return mService.getPersonObs(login)
                .compose(new ResponseCallTransformer<>())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(PersonDto::new);
    }

    //endregion
}
