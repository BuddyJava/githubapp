package com.timofeev.githubapp.core;

public interface IView {
    void showProgress();
    void hideProgress();
    void showError(Throwable e);
}
