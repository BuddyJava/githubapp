package com.timofeev.githubapp.core;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public interface IPresenter {

    void bindView(@NonNull IView view);

    void unbindView();

    @Nullable
    IView getView();

}
