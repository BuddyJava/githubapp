package com.timofeev.githubapp.core;

import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import rx.subscriptions.CompositeSubscription;


public abstract class BasePresenter<V extends IView> implements IPresenter {

    @Nullable
    private V view;

    public BasePresenter() {
        initComponent();
    }

    @Override
    @CallSuper
    @SuppressWarnings("unchecked")
    public void bindView(@NonNull IView view) {
        this.view = (V) view;
    }

    @Override
    public void unbindView() {
        view = null;
    }

    @Nullable
    @Override
    public V getView() {
        return view;
    }

    public abstract void initComponent();
}
