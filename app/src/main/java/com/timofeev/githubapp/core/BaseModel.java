package com.timofeev.githubapp.core;

import com.timofeev.githubapp.data.managers.DataManager;
import com.timofeev.githubapp.di.components.DaggerModelComponent;

import javax.inject.Inject;

public class BaseModel {

    @Inject
    protected DataManager mDataManager;

    public BaseModel() {
        DaggerModelComponent.create().inject(this);
    }
}
