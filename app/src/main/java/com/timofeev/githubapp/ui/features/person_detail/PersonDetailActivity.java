package com.timofeev.githubapp.ui.features.person_detail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.timofeev.githubapp.R;
import com.timofeev.githubapp.data.storage.PersonDto;
import com.timofeev.githubapp.di.DaggerService;
import com.timofeev.githubapp.di.scopes.DetailScope;
import com.timofeev.githubapp.ui.base.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Provides;


public class PersonDetailActivity extends BaseActivity implements IDetailView {

    //region ============= FBI ===============
    @BindView(R.id.bio_person)
    EditText mBioEt;
    @BindView(R.id.email_person)
    EditText mEmailEt;
    @BindView(R.id.company_person)
    EditText mCompanyEt;
    @BindView(R.id.location_person)
    EditText mLocationEt;
    @BindView(R.id.avatar_person)
    ImageView mAvatar;
    //endregion

    private static final String EXTRA_PERSON_LOGIN = "com.timofeev.githubapp.login_person";

    @Inject
    PersonDetailPresenter mPresenter;

    public static Intent newIntent(Context context, String login) {
        Intent loginIntent = new Intent(context, PersonDetailActivity.class);
        loginIntent.putExtra(EXTRA_PERSON_LOGIN, login);
        return loginIntent;
    }


    //region ============= Life ===============
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_detail);
        ButterKnife.bind(this);
        mPresenter.bindView(this);
//        mPresenter.initView(login);

    }

    @Override
    protected void onStart() {
        mPresenter.initView(getIntent().getStringExtra(EXTRA_PERSON_LOGIN));
        super.onStart();
    }

    @Override
    protected void onStop() {
        mPresenter.unbindView();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        DaggerService.unregisterScope(DetailScope.class);
        super.onDestroy();
    }
    //endregion

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {

            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupToolbar(String name) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);


        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(name);
        }
    }

    //region ============= IDetailView ===============

    @Override
    public void showPersonInfo(PersonDto person) {

        setupToolbar(person.getName());
        checkNull(mLocationEt, person.getLocation());
        checkNull(mEmailEt, person.getEmail());
        checkNull(mBioEt, person.getBio());
        checkNull(mCompanyEt, person.getCompany());
        Picasso.with(this)
                .load(person.getAvatar())
                .into(mAvatar);
    }

    private void checkNull(EditText view, String info) {
        if (info != null) {
            view.setText(info);
        }
    }
    //endregion

    //region ============= DI ===============

    @Override
    public void initComponent() {
        ((Component) DaggerService.getComponent(PersonDetailActivity.Component.class,
                DaggerPersonDetailActivity_Component.class,
                new Module())).inject(this);
    }

    @dagger.Module
    public class Module {
        @Provides
        @DetailScope
        PersonDetailPresenter providePersonDetailPresenter() {
            return new PersonDetailPresenter();
        }
    }

    @dagger.Component(modules = Module.class)
    @DetailScope
    interface Component {
        void inject(PersonDetailActivity activity);
    }

    //endregion
}
