package com.timofeev.githubapp.ui.features.person_detail;

import com.timofeev.githubapp.core.IPresenter;

public interface IDetailPresenter extends IPresenter{
    void initView(String login);
}
