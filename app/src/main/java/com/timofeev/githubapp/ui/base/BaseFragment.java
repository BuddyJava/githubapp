package com.timofeev.githubapp.ui.base;

import android.content.Context;
import android.os.Build;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.View;

@SuppressWarnings("unused")
public abstract class BaseFragment extends Fragment {

    public <T extends View> T $(@IdRes int id) {
        //noinspection unchecked,ConstantConditions
        return (T) getView().findViewById(id);
    }

    @Override
    public Context getContext() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            return super.getContext();
        else
            return getActivity();
    }

    @Nullable
    protected BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }


    public void showSnackbar(@StringRes int res) {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null)
            baseActivity.showSnackbar(res);
    }

    public void showSnackbar(@Nullable String s) {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null)
            baseActivity.showSnackbar(s);
    }

    public void showProgress() {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null)
            getBaseActivity().showProgress();
    }

    public void hideProgress() {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null)
            getBaseActivity().hideProgress();
    }

    public void onBackPressed() {
        getActivity().onBackPressed();
    }
}
