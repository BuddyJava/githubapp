package com.timofeev.githubapp.ui.features.person_detail;

import com.timofeev.githubapp.core.IView;
import com.timofeev.githubapp.data.storage.PersonDto;


public interface IDetailView extends IView{
    void showPersonInfo(PersonDto person);
}
