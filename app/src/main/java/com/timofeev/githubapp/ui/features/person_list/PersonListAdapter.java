package com.timofeev.githubapp.ui.features.person_list;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.timofeev.githubapp.R;
import com.timofeev.githubapp.data.storage.PeopleDto;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.timofeev.githubapp.ui.features.person_list.PersonListAdapter.PersonViewHolder.*;

public class PersonListAdapter extends RecyclerView.Adapter<PersonListAdapter.PersonViewHolder> {


    private List<PeopleDto> mList = new ArrayList<>();
    private Context mContext;
    private PersonHolderListener mListener;

    public PersonListAdapter(Context context, PersonHolderListener listener) {
        mContext = context;
        mListener = listener;

    }

//    private void initComponent() {
//        ((PersonListActivity.Component) DaggerService.getComponent(PersonListActivity.Component.class,
//                DaggerPersonListActivity_Component.class,
//                new PicassoCacheModule())).inject(this);
//    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.person_list_content, parent, false);

        return new PersonViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(PersonViewHolder holder, int position) {
        PeopleDto person = mList.get(position);
        holder.mLogin.setText(person.getLogin());
        Picasso.with(mContext)
                .load(person.getUrlAvatar())
                .resize(80, 80)
                .into(holder.mAvatar);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void addItem(PeopleDto peopleDto) {
        mList.add(peopleDto);
        notifyDataSetChanged();
    }

    public void clear() {
        mList.clear();
    }

    public static class PersonViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private PersonHolderListener mListener;

        @BindView(R.id.image_person)
        CircleImageView mAvatar;
        @BindView(R.id.login_person)
        TextView mLogin;

        public PersonViewHolder(View itemView, PersonHolderListener listener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mListener = listener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mListener != null) {
                mListener.OnItemClick(mLogin.getText().toString());
            }
        }

        public interface PersonHolderListener {
            void OnItemClick(String login);
        }
    }
}
