package com.timofeev.githubapp.ui.features.person_detail;


import com.timofeev.githubapp.core.BasePresenter;
import com.timofeev.githubapp.data.storage.PersonDto;
import com.timofeev.githubapp.di.DaggerService;
import com.timofeev.githubapp.di.scopes.DetailScope;
import com.timofeev.githubapp.utills.L;

import javax.inject.Inject;

import dagger.Component;
import dagger.Module;
import dagger.Provides;
import rx.Subscriber;
import rx.Subscription;

public class PersonDetailPresenter extends BasePresenter<PersonDetailActivity> implements IDetailPresenter {

    @Inject
    PersonDetailModel mModel;



    //region ============= IDetailPresenter ===============

    @Override
    public void initView(String login) {

        if (getView() != null) {
            getView().showProgress();
            subscribePersonObs(login);
        }
    }


    //endregion

    private Subscription subscribePersonObs(String login) {
        return mModel.getPersonObs(login)
                .subscribe(new Subscriber<PersonDto>() {
                    @Override
                    public void onCompleted() {
                        getView().hideProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        L.e(e.toString());
                        getView().hideProgress();
                        getView().showError(e);
                    }

                    @Override
                    public void onNext(PersonDto personDto) {
                        getView().showPersonInfo(personDto);
                    }
                });
    }
    //region ============= DI ===============

    @Override
    public void initComponent() {
        ((Component) DaggerService.getComponent(Component.class,
                DaggerPersonDetailPresenter_Component.class,
                new Module())).inject(this);
    }
    @dagger.Module
    public class Module {
        @Provides
        @DetailScope
        PersonDetailModel providePersonDetailModel() {
            return new PersonDetailModel();
        }
    }

    @dagger.Component(modules = Module.class)
    @DetailScope
    interface Component {
        void inject(PersonDetailPresenter presenter);
    }

    //endregion
}
