package com.timofeev.githubapp.ui.features.person_list;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;


import com.squareup.picasso.Picasso;
import com.timofeev.githubapp.di.DaggerService;
import com.timofeev.githubapp.di.Injector;
import com.timofeev.githubapp.di.components.AppComponent;
import com.timofeev.githubapp.di.scopes.ListScope;
import com.timofeev.githubapp.ui.base.BaseActivity;
import com.timofeev.githubapp.R;
import com.timofeev.githubapp.ui.features.person_detail.PersonDetailActivity;
import com.timofeev.githubapp.utills.EndlessRecyclerViewScrollListener;

import javax.inject.Inject;

import dagger.Provides;

public class PersonListActivity extends BaseActivity implements IListView {

    @Inject
    PersonListPresenter mPresenter;


    private PersonListAdapter mAdapter;
    private SwipeRefreshLayout mRefreshLayout;
    private LinearLayoutManager mLayoutManager;
    private EndlessRecyclerViewScrollListener mListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_list);

        mPresenter.bindView(this);
        setupToolbar();
        setupRefreshLayout();

        View recyclerView = findViewById(R.id.person_list);
        assert recyclerView != null;
        setupRecyclerView((RecyclerView) recyclerView);


        mPresenter.initView();
    }

    @Override
    protected void onDestroy() {
        DaggerService.unregisterScope(ListScope.class);
        mPresenter.unbindView();
        super.onDestroy();
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new PersonListAdapter(this, login -> mPresenter.onClickItem(login));
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);
        mListener = new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                mPresenter.loadMore(page);
            }
        };
        recyclerView.addOnScrollListener(mListener);
    }


    private void setupToolbar() {
        Toolbar toolbar = $(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());
    }

    private void setupRefreshLayout() {
        mRefreshLayout = $(R.id.refresh_layout);
        mRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        mRefreshLayout.setOnRefreshListener(() -> mPresenter.onRefresh());
    }

    //region ============= IListVIew ===============


    @Override
    public void startDetailActivity(String login) {
        startActivity(PersonDetailActivity.newIntent(this, login));
    }


    @Override
    public void stopRefresh() {
        mRefreshLayout.setRefreshing(false);
        mListener.resetState();
    }


    //endregion

    public PersonListAdapter getAdapter() {
        return mAdapter;
    }

    //region ============= DI ===============

    @Override
    public void initComponent() {
        ((Component) DaggerService.getComponent(Component.class,
                DaggerPersonListActivity_Component.class,
                Injector.getAppComponent(),
                new Module())).inject(this);
    }

    @dagger.Module
    public class Module {
        @Provides
        @ListScope
        PersonListPresenter providePersonListPresenter() {
            return new PersonListPresenter();
        }
    }

    @dagger.Component(dependencies = AppComponent.class, modules = {Module.class})
    @ListScope
    interface Component {
        void inject(PersonListActivity activity);

//        void inject(PersonListAdapter list);
    }

    //endregion
}
