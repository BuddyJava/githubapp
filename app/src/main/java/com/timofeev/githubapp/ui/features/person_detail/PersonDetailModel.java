package com.timofeev.githubapp.ui.features.person_detail;

import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.timofeev.githubapp.core.BaseModel;
import com.timofeev.githubapp.data.storage.PersonDto;

import rx.Observable;

public class PersonDetailModel extends BaseModel {
    public Observable<PersonDto> getPersonObs(String login){
        return mDataManager.getPersonObs(login);
    }
}
