package com.timofeev.githubapp.ui.features.person_list;

import com.timofeev.githubapp.core.IPresenter;

public interface IListPresenter extends IPresenter{

    void onClickItem(String login);
    void initView();
    void onRefresh();
    void loadMore(int page);
}
