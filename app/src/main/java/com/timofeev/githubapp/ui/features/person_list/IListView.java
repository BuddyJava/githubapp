package com.timofeev.githubapp.ui.features.person_list;

import com.timofeev.githubapp.core.IView;


public interface IListView extends IView {

    void startDetailActivity(String login);
    void stopRefresh();
    PersonListAdapter getAdapter();
}
