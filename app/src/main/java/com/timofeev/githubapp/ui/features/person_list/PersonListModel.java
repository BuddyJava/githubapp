package com.timofeev.githubapp.ui.features.person_list;

import com.timofeev.githubapp.core.BaseModel;
import com.timofeev.githubapp.data.storage.PeopleDto;

import rx.Observable;

public class PersonListModel extends BaseModel {

    public Observable<PeopleDto> getPeopleObs(int page){
        return mDataManager.getPeopleObs(page);
    }

}
