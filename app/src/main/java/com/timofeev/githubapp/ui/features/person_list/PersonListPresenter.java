package com.timofeev.githubapp.ui.features.person_list;

import com.timofeev.githubapp.core.BasePresenter;
import com.timofeev.githubapp.data.storage.PeopleDto;
import com.timofeev.githubapp.di.DaggerService;
import com.timofeev.githubapp.di.scopes.ListScope;
import com.timofeev.githubapp.utills.L;

import javax.inject.Inject;

import dagger.Provides;
import rx.Subscriber;

public class PersonListPresenter extends BasePresenter<PersonListActivity> implements IListPresenter {

    @Inject
    PersonListModel mModel;

    public PersonListPresenter() {
    }

    //region ============= IListPresenter ===============
    @Override
    public void initView() {

        if (getView() != null) {
            getView().showProgress();
            subscribePeopleObs(1, new LoadSubscriber());
        }
    }

    @Override
    public void onClickItem(String login) {
        if (getView() != null) {

            getView().startDetailActivity(login);

        }
    }

    @Override
    public void onRefresh() {
        L.e("onRefresh");
        if (getView() != null) {
            getView().getAdapter().clear();
            subscribePeopleObs(1, new RefreshSubscriber());
        }
    }

    @Override
    public void loadMore(int page) {
        if (getView() != null) {
            subscribePeopleObs(page, new LoadSubscriber());
        }
    }
    //endregion

    private void subscribePeopleObs(int page, Subscriber subscriber) {
        mModel.getPeopleObs(page)
                .subscribe(subscriber);


    }

    //region ============= SUBSCRIBER ===============
    private class LoadSubscriber extends Subscriber<PeopleDto> {
        @Override
        public void onCompleted() {
            getView().hideProgress();
        }

        @Override
        public void onError(Throwable e) {
            getView().hideProgress();
            getView().showError(e);
        }

        @Override
        public void onNext(PeopleDto peopleDto) {
            getView().getAdapter().addItem(peopleDto);
        }
    }

    private class RefreshSubscriber extends Subscriber<PeopleDto> {
        @Override
        public void onCompleted() {
            getView().stopRefresh();
        }

        @Override
        public void onError(Throwable e) {
            getView().stopRefresh();
            getView().showError(e);
        }

        @Override
        public void onNext(PeopleDto peopleDto) {
            getView().getAdapter().addItem(peopleDto);
        }
    }
    //endregion

    //region ============= DI ===============

    @Override
    public void initComponent() {
        ((Component) DaggerService.getComponent(Component.class,
                DaggerPersonListPresenter_Component.class,
                new Module())).inject(this);
    }

    @dagger.Module
    public class Module {
        @Provides
        @ListScope
        PersonListModel providePersonListModel() {
            return new PersonListModel();
        }
    }

    @dagger.Component(modules = Module.class)
    @ListScope
    interface Component {
        void inject(PersonListPresenter presenter);
    }

    //endregion
}
