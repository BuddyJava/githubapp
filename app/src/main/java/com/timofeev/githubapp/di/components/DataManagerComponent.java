package com.timofeev.githubapp.di.components;

import com.timofeev.githubapp.data.managers.DataManager;
import com.timofeev.githubapp.di.components.AppComponent;
import com.timofeev.githubapp.di.modules.LocalModule;
import com.timofeev.githubapp.di.modules.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(dependencies = AppComponent.class,
        modules = {NetworkModule.class, LocalModule.class})
public interface DataManagerComponent {
    void inject(DataManager dataManager);
}
