package com.timofeev.githubapp.di.modules;


import com.timofeev.githubapp.data.managers.DataManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ModelModule {

    @Provides
    @Singleton
    DataManager provideDataManager(){
        return DataManager.getInstance();
    }

}
