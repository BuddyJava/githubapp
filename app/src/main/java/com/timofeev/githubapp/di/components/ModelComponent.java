package com.timofeev.githubapp.di.components;


import com.timofeev.githubapp.core.BaseModel;
import com.timofeev.githubapp.di.modules.ModelModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ModelModule.class)
public interface ModelComponent {
    void inject(BaseModel model);
}
