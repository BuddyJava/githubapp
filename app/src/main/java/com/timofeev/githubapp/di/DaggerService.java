package com.timofeev.githubapp.di;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;


import com.timofeev.githubapp.utills.L;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.Nonnull;
import javax.inject.Scope;

@SuppressWarnings("unchecked")
public class DaggerService {

    private static Map<String, Object> sComponentMap = new HashMap<>();

    @NonNull
    public static <T> T getComponent(Class componentClass, Class daggerClass,
                                     Object... dependencies) {
        return getComponent(null, componentClass, daggerClass, dependencies);
    }

    public static <T> T getComponent(String key, Class componentClass, Class daggerClass,
                                     Object... dependencies) {
        T component = (T) getComponent(key, componentClass);
        if (component != null) return component;

        component = createComponent(daggerClass, dependencies);
        assert component != null;

        registerComponent(key, component);
        return component;
    }

    public static void unregisterScope(Class<? extends Annotation> scopeAnnotation) {
        L.d("remove scope ", scopeAnnotation);
        Iterator<Map.Entry<String, Object>> iterator = sComponentMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Object> entry = iterator.next();
            if (entry.getKey().contains(scopeAnnotation.getName())) {
                L.d("removed scope ", entry.getKey());
                iterator.remove();
            }
        }
    }

    public static void unregisterOne(String key, Class componentClass) {
        Class<? extends Annotation> scopeAnnotation = getScopeAnnotation(componentClass);
        if (scopeAnnotation != null) {
            sComponentMap.remove(componentClass.getName() + scopeAnnotation.getName() + key);
        }
    }

    @Nullable
    private static Class<? extends Annotation> getScopeAnnotation(Class componentClass) {
        Annotation[] annotations = componentClass.getAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation.annotationType().isAnnotationPresent(Scope.class)) {
                return annotation.annotationType();
            }
        }
        return null;
    }


    private static <T> T createComponent(Class daggerClass, Object... dependencies) {
        try {
            Object builder = daggerClass.getMethod("builder").invoke(null);
            for (Method method : builder.getClass().getDeclaredMethods()) {
                Class<?>[] params = method.getParameterTypes();
                if (params.length != 1) continue;
                Class<?> dependencyClass = params[0];
                for (Object dependency : dependencies) {
                    if (dependencyClass.isAssignableFrom(dependency.getClass())) {
                        method.invoke(builder, dependency);
                        break;
                    }
                }
            }
            return (T) builder.getClass().getMethod("build").invoke(builder);
        } catch (Exception e) {
            L.e(e);
            return null;
        }
    }

    @Nullable
    private static <T> T getComponent(String objectKey, Class<T> componentClass) {
        Class<? extends Annotation> scopeAnnotation = getScopeAnnotation(componentClass);
        if (scopeAnnotation != null) {
            String key = componentClass.getName() + scopeAnnotation.getName();
            key += objectKey == null ? "" : objectKey;
            return (T) sComponentMap.get(key);
        }
        return null;
    }

    private static void registerComponent(@Nullable String objectKey, @Nonnull Object component) {
        Class componentClass = component.getClass().getInterfaces()[0];
        Class<? extends Annotation> scopeAnnotation = getScopeAnnotation(componentClass);
        if (scopeAnnotation != null) {
            String key = componentClass.getName() + scopeAnnotation.getName();
            key += objectKey == null ? "" : objectKey;
            sComponentMap.put(key, component);
        }
    }
}

