package com.timofeev.githubapp.di;


import android.content.Context;
import android.support.annotation.NonNull;

import com.timofeev.githubapp.di.components.AppComponent;
import com.timofeev.githubapp.di.components.DaggerAppComponent;
import com.timofeev.githubapp.di.modules.AppModule;

public class Injector {
    private static AppComponent appComponent;

    public static void init(@NonNull Context context) {
        appComponent = DaggerAppComponent
                .builder()
                .appModule(new AppModule(context))
                .build();
    }

    @NonNull
    public static AppComponent getAppComponent() {
        if (appComponent == null) {
            throw new RuntimeException("AppComponent is NULL!");
        }
        return appComponent;
    }
}
