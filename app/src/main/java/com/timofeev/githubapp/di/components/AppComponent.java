package com.timofeev.githubapp.di.components;

import android.content.Context;

import com.timofeev.githubapp.di.modules.AppModule;

import dagger.Component;

@Component(modules = AppModule.class)
public interface AppComponent {
    Context getContext();
}
