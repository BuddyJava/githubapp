package com.timofeev.githubapp;

import android.app.Application;
import android.content.Context;

import com.timofeev.githubapp.di.Injector;

public class App extends Application{

    private static Context sContext;

    @Override
    public void onCreate() {
        super.onCreate();

        sContext = getApplicationContext();
        initService();
    }

    private void initService() {
        Injector.init(this);
    }

    public static Context getContext(){
        return sContext;
    }
}
