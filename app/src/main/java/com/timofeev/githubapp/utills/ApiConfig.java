package com.timofeev.githubapp.utills;

public class ApiConfig {
    public static final String BASE_URL = "https://api.github.com/";
    public static final long MAX_CONNECTION_TIMEOUT = 5000;
    public static final long MAX_READ_TIMEOUT = 5000;
    public static final long MAX_WRITE_TIMEOUT = 5000;

}
